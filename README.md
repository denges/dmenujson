# dmenuJSON

Python wrapper for dmenu that shows options from a JSON file and executes respective commands after selection. Takes as argument a JSON file such as `example.json` containing a list of commands.

## Motivation
I am an enthusiastic i3wm user that uses Visual Studio Code for 80\% of my work. The command palette (Ctrl+Shift+P) in VS Code is great and I thought it would be cool to have something similar for i3 and well this is what this is. Put all your commands in a JSON file and this gives you a command palette.

![dmenujsonDemo.gif](https://gitlab.com/denges/dmenujson/-/raw/main/dmenujsonDemo.gif)

## Example

Simply run 

`python3 dmenuJSON.py example.json`

to test the program. You will have to have dmenu installed for anything to happen and the i3 window manager installed for the example actions to work.

## Integration into i3

Add something like this to your i3 config

`bindsym $mod+Shift+d exec python3 [path to dmenuJSON.py] [path to JSON file]`

## Organization of JSON file and options

see example.json which includes most implemented functions. Only one field is mandatory, namely `options`, which defines the options to be shown to user. `options` is of list of entries where each entry has the following options:

* __name__ (optional): Text to be displayed for option (if not provided value of command / file is shown)
* __command__ (optional [^1]): command to be executed when option is chosen
* __file__ (optional [^1]): path of script to be executed relative to directory provided in JSON file. If `file-directory` is not provided, path is relative to the directory the JSON file is in.


Apart from `options` on the root JSON level a number of parameters can be changed, i.e.

 * __prompt__ (optional): text to be displayed in front of options
 * __dmenu-options__ (optional): arguments to be passed to `dmenu` (mostly esthetic), e.g. `["-l", "10", "-i"]` for `dmenu` showing 10 lines and the search being case-insensitive.
 * __file-directory__ (optional): path where to look for scripts (default: directory of JSON file)
 * __order__ (optional): "asc" or "desc" for ascending resp. descending order of entries (default and any other value will maintain order of options in JSON)
 * __terminal-emulator__ (optional): specifies what to preface commands / scripts set to run in terminal (default: "gnome-terminal --")
 * __options__ (required): list of entries to be shown (see above for options of entries)

 [^1]: Either `command` or `file` have to specified but not both.
